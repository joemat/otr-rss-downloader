package de.elinja.tools.otr.journal

import org.scalatest._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import java.nio.file.Files
import java.nio.file.Paths
import de.elinja.tools.otr.DownloadStatus._
import java.io.File

@RunWith(classOf[JUnitRunner])
class TestJournal extends FlatSpec {

  val journalfile = "build/tmp/journal.jf"

  def loadEmptyJournal(): JournalFile = {
    Files.deleteIfExists(Paths.get(journalfile))
    // ensure the directory exists
    new File(journalfile).getParentFile.mkdirs()
    new JournalFile(journalfile)
  }

  "Loading a not existing journal" should "create an empty one" in {
    val items = loadEmptyJournal().items
    assert(items.isEmpty)
  }

  "Adding a new entry to an empty list" should "result in a list with one element" in {
    val jf = loadEmptyJournal()
    jf.updateItem("foo", PROCESSING)
    val items = jf.items
    assert(items.length == 1)
  }

  "Adding the same entry twice to an empty list" should "result in a list with one element" in {
    val jf = loadEmptyJournal()
    jf.updateItem("foo", PROCESSING)
    jf.updateItem("foo", PROCESSING)
    val items = jf.items
    assert(items.length == 1)
  }

  "Adding two entries to an empty list" should "result in a list with two updated elements" in {
    val jf = loadEmptyJournal()
    jf.updateItem("foo", PROCESSING)
    jf.updateItem("bar", PROCESSING)
    val items = jf.items
    assert(items.length == 2)
    items.foreach { item => assert(item.updated == true) }
  }

  "Saving and loading a file" should "have the same entries but marked outdated" in {
    val jf = loadEmptyJournal()
    jf.updateItem("foo", PROCESSING)
    jf.updateItem("bar", PROCESSING)

    jf.save()

    val jfLoaded = new JournalFile(journalfile)
    val itemsLoaded = jfLoaded.items
    assert(2 == itemsLoaded.filter(item => { item.updated == false }).length)
    assert(1 == itemsLoaded.filter(item => { item.downloadUrl == "bar" }).length)
    assert(1 == itemsLoaded.filter(item => { item.downloadUrl == "foo" }).length)

    val fooItem = itemsLoaded.filter(item => { item.downloadUrl == "foo" }).head
    assert(fooItem.status == "PROCESSING")
  }
}
