
package de.elinja.tools.otr
import org.scalatest._
import scala.collection.mutable.Stack
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import java.nio.file.Files
import java.nio.file.Paths
import com.typesafe.config.ConfigFactory

@RunWith(classOf[JUnitRunner])
class TestMailSender extends FlatSpec {

  "Mail content" should "be prefix + status + postfix" in {
    val cfg = ConfigFactory.load();

    val status: List[String] = "abc" :: ("def" :: Nil);
    val ms: MailSender = new MailSender(cfg);

    val expected = "I have downloaded these movies:\n\rabc\n\rdef\n\rSee you later\n\r";
    assert(expected == ms.createMailContent(cfg, status));
  }
}
