
package de.elinja.tools.otr
import org.scalatest._
import scala.collection.mutable.Stack
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite
import java.nio.file.Files
import java.nio.file.Paths

@RunWith(classOf[JUnitRunner])
class TestOtrRssDownloader extends FlatSpec {

  "Loading test1.conf" should "return url src/test/resources/test1/test1_list.xml" in {
    val otrRssDownloader = new OtrRssDownloader("src/test/resources/test1/test1.conf");
    assert(otrRssDownloader.rssReader.cfg.getString("url") === "src/test/resources/test1/test1_list.xml")
  }

  "loadRssXmlAndGetItems for test1.conf" should "return 2 items" in {
    val otrRssDownloader = new OtrRssDownloader("src/test/resources/test1/test1.conf");
    val items = otrRssDownloader.rssReader.getDownloadRunnables()
    assert(items.length == 2)
  }

  "createDownloadRunnablesForItems" should "create 2 runnables with correct url and filename" in {

    val otrRssDownloader = new OtrRssDownloader("src/test/resources/test1/test1.conf");
    val runnables = otrRssDownloader.rssReader.getDownloadRunnables()
    assert(runnables.length == 2)

    val firstRunnable = runnables.head
    assert(firstRunnable.downloadUrl === "src/test/resources/test1/test1_file1.txt")
    assert(firstRunnable.targetFilename === "build/tmp/test1/test1_file1.txt")

    val secondRunnable = runnables.tail.head
    assert(secondRunnable.downloadUrl === "src/test/resources/test1/test1_file2.txt")
    assert(secondRunnable.targetFilename === "build/tmp/test1/test1_file2.txt")
  }

  "Configname of /foo/bar/thud.conf" should "be thud" in {
    assert("thud" === new OtrRssDownloader("/foo/bar/thud.conf").configName)
  }

  "Configname of foo/bar/grunt.conf" should "be grunt" in {
    assert("grunt" === new OtrRssDownloader("foo/bar/grunt.conf").configName)
  }

  "Configname of bar.conf" should "be bar" in {
    assert("bar" === new OtrRssDownloader("bar.conf").configName)
  }

  "Configname of foo" should "be foo" in {
    assert("foo" === new OtrRssDownloader("foo").configName)
  }

}
