package de.elinja.tools.otr

object DownloadStatus extends Enumeration {
  type DownloadStatus = Value
  val NEW, WAITING, PROCESSING, ERROR, COMPLETED = Value
}
