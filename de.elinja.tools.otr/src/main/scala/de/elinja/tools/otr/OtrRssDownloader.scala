package de.elinja.tools.otr;

import com.github.kevinsawicki.http.HttpRequest
import scala.xml.XML
import java.io.StringWriter
import scala.xml.Elem
import com.typesafe.config.ConfigFactory
import java.io.File
import scala.collection.JavaConversions._
import scala.collection.convert.WrapAsScala.enumerationAsScalaIterator
import java.util.concurrent.Executors
import java.io.IOException
import scala.collection.mutable.ListBuffer
import java.util.concurrent.TimeUnit
import java.util.concurrent.ExecutorService
import de.elinja.tools.otr.journal.JournalFile
import de.elinja.tools.otr.DownloadStatus._
import java.text.MessageFormat

class OtrRssDownloader(configFile: String) {

  println("Loading configuration from: " + configFile)
  val cfg = ConfigFactory.parseFile(new File(configFile)).withFallback(ConfigFactory.load())
  val configName = getConfigName(configFile)
  val targetDir = cfg.getString("targetdir")

  private var downloadRunnables: List[DownloadRunnable] = Nil
  val threadPool = Executors.newFixedThreadPool(cfg.getInt("maxthreads"))
  val rssReader = new RssReader(cfg, targetDir);
  val journal = new JournalFile(targetDir + "/." + configName + "-journal.xml")

  private def getConfigName(configFile: String): String = {
    val fileNameMatcher = "^(.*[/\\\\])?([^/\\\\\\.]+)(\\.[^\\.]*)?$".r
    val fileNameMatcher(dir, basename, ext) = configFile
    return basename
  }

  def updateStatusFromJournal(runnables: List[DownloadRunnable]) = {
    runnables.foreach { runnable =>
      runnable.setInitialStatus(journal.getStatusOf(runnable.downloadUrl))
    }
  }

  def startDownloadThreads() = {
    downloadRunnables = rssReader.getDownloadRunnables()
    updateStatusFromJournal(downloadRunnables);

    downloadRunnables.foreach { runnable =>
      if (runnable.status != COMPLETED) {

        println(MessageFormat.format(cfg.getString("messages.starting"), runnable.downloadUrl));
        journal.updateItem(runnable.downloadUrl, PROCESSING)
        threadPool.execute(runnable);
      } else {
        println(MessageFormat.format(cfg.getString("messages.alreadydownloaded"), runnable.downloadUrl));
      }
    }

    threadPool.shutdown()
    journal.save()
  }

  def statusOfAnyDownloadHasChanged(): Boolean = {
    !downloadRunnables.filter { r => r.hasStatusChanged }.isEmpty;
  }

  def updateJournalFileAndGetStatus(): List[String] = {
    var statusMessage = List[String]();

    downloadRunnables.foreach { runnable =>
      statusMessage = runnable.getStatus(cfg) :: statusMessage;
      journal.updateItem(runnable.downloadUrl, runnable.status)
    }
    journal.save()
    return statusMessage;
  }

  def waitForCompletion() = {
    while (!threadPool.awaitTermination(5, TimeUnit.SECONDS)) {
      val status = updateJournalFileAndGetStatus()
      printStatus(status);
    }
  }

  def printStatus(status: List[String]) = {
    println();
    println(cfg.getString("messages.status.header.header"));
    println(cfg.getString("messages.status.header.processing"));
    status.foreach { line => println(line) }
    println();
  }
}
