package de.elinja.tools.otr
import courier._
import com.typesafe.config.Config
import scala.concurrent.Await
import scala.concurrent.Await
import scala.concurrent.duration._
import javax.mail.internet.InternetAddress
import courier.Session
import javax.mail.Session
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success
import scala.util.Failure
import java.text.MessageFormat

class MailSender(val cfg: Config) {

  def sendStatusMail(status: List[String]): Boolean = {

    try {
      if (cfg.getString("smtp.server") != null) {
        try {
          trySendMail(status)
          return true;
        } catch {
          case e: Exception => MessageFormat.format(cfg.getString("messages.mail.error"), e.getLocalizedMessage);
        }
      }
    } catch {
      case e: Exception => // do nothing but return false
    }

    return false;
  }

  private def trySendMail(status: List[String]) = {
    val mailer = createMailer(cfg);
    val mailContent = createMailContent(cfg, status);

    val from = new InternetAddress(cfg.getString("smtp.from"))
    val to = new InternetAddress(cfg.getString("smtp.to"))
    val subject = cfg.getString("smtp.subject")

    val envelope: Envelope = Envelope.from(from)
      .to(to)
      .subject(subject)
      .content(Text(mailContent));

    val future = mailer(envelope);

    future.onComplete {
      case Success(value) => println(MessageFormat.format(cfg.getString("messages.mail.sentto"), to));
      case Failure(e) => MessageFormat.format(cfg.getString("messages.mail.error"), e.getLocalizedMessage);
    }
    Await.ready(future, 30.seconds)
  }

  private def createMailer(cfg: Config): Mailer = {
    val needsAuth = cfg.getBoolean("smtp.needsAuth");
    var sessionBuilder = Mailer(cfg.getString("smtp.server"), cfg.getInt("smtp.port"));

    if (needsAuth) {
      sessionBuilder = sessionBuilder.auth(needsAuth)
        .as(cfg.getString("smtp.user"), cfg.getString("smtp.password"));
    } else {
      sessionBuilder = sessionBuilder.auth(false);
    }

    sessionBuilder.startTtls(cfg.getBoolean("smtp.enablestarttls")) //
      .debug(cfg.getBoolean("smtp.debug"))()
  }

  def createMailContent(cfg: Config, status: List[String]): String = {
    val lines: List[String] = cfg.getString("smtp.content.prefix") :: status ::: (cfg.getString("smtp.content.postfix") :: Nil);
    lines.fold("")((a, b) => a + b + "\n\r");
  }
}
