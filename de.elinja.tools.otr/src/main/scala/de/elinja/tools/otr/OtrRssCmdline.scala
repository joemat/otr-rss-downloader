package de.elinja.tools.otr

import java.io.File
import java.nio.channels.FileLock
import java.io.RandomAccessFile
import java.io.IOException
import java.nio.channels.FileChannel
import java.nio.file.Files

object OtrRssCmdline {

  // configuration filename used if no parameter is given
  val DEFAULT_CONFIG_FILE = "/etc/otr-rss-downloader/otr-rss-downloader.conf"

  def main(args: Array[String]): Unit = {

    var configFile = DEFAULT_CONFIG_FILE;

    if (args.length > 0) {
      configFile = args(0)
    } else {
      println("No parameters given - using default configuration file " + configFile);
    }

    val otrRssDownloader = new OtrRssDownloader(configFile);

    val lock = new LockFile(otrRssDownloader.configName, otrRssDownloader.targetDir)

    lock.aquire()
    otrRssDownloader.startDownloadThreads()
    otrRssDownloader.waitForCompletion()
    val status = otrRssDownloader.updateJournalFileAndGetStatus()
    lock.release()

    println();
    println(otrRssDownloader.cfg.getString("messages.status.header.header"));
    println(otrRssDownloader.cfg.getString("messages.status.header.done"));
    status.foreach { line => println(line) }
    println();
    println(otrRssDownloader.cfg.getString("messages.status.header.header"));
    println();
    sendMailIfNeeded(otrRssDownloader, status);
    println();
  }

  def sendMailIfNeeded(otrRssDownloader: de.elinja.tools.otr.OtrRssDownloader, status: List[String]) = {
    if (!otrRssDownloader.statusOfAnyDownloadHasChanged()) {

      println(otrRssDownloader.cfg.getString("messages.mail.nothingchanged"));
    } else {
      val mailSender = new MailSender(otrRssDownloader.cfg);
      if (mailSender.sendStatusMail(status)) {
        println(otrRssDownloader.cfg.getString("messages.mail.sent"));
      } else {
        println(otrRssDownloader.cfg.getString("messages.mail.notconfigured"));
      }
    }
  }
}
