package de.elinja.tools.otr

import com.github.kevinsawicki.http.HttpRequest
import java.io.File
import java.nio.file.Paths
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.io.IOException
import java.nio.file.Path
import DownloadStatus._
import com.typesafe.config.Config
import java.text.MessageFormat

class DownloadRunnable(val downloadUrl: String, val targetFilename: String) extends Runnable {

  val targetFile = new File(targetFilename)
  var completeLength: Long = -1;
  var status = WAITING
  var initialStatus = WAITING
  var exception: Exception = null;

  override def run() = {
    try {
      updateStatus(PROCESSING)
      updateStatus(tryGetFile())
    } catch {
      case e: Exception => updateStatus(ERROR); exception = e;
    }
  }

  private def hasHttpPrefix(url: String): Boolean = {
    url.startsWith("http://") || url.startsWith("https://")
  }

  private def tryGetFile(): DownloadStatus = {
    if (hasHttpPrefix(downloadUrl)) {
      downloadFile()
    } else {
      copyFile()
    }
  }

  private def copyFile(): DownloadStatus = {
    val srcFile = Paths.get(downloadUrl)
    val dstFile = targetFile.toPath()
    val resultFile = Files.copy(srcFile, dstFile, StandardCopyOption.COPY_ATTRIBUTES);
    completeLength = srcFile.toFile().length()
    COMPLETED
  }

  private def downloadFile(): DownloadStatus = {
    val request = HttpRequest.get(downloadUrl);
    if (!request.ok()) {
      throw new IOException("Got HTTP result code " + request.code());
    }
    completeLength = request.contentLength()
    request.receive(targetFile);
    COMPLETED
  }

  def updateStatus(newStatus: DownloadStatus) = {
    status = newStatus;
  }

  def setInitialStatus(initStatus: DownloadStatus) = {
    initialStatus = initStatus;
    status = initStatus;
  }

  def hasStatusChanged: Boolean = {
    initialStatus != status
  }

  def getStatus(cfg: Config): String = {
    val messageTemplate = cfg.getString("messages.status." + status);

    if (targetFile.exists() && status == PROCESSING) {
      val bytes = targetFile.length()
      MessageFormat.format(messageTemplate, targetFile, status, bytes.toString(), completeLength.toString());
    } else if (status == ERROR) {
      val errorMessage = exception.getMessage();
      MessageFormat.format(messageTemplate, targetFile, status, errorMessage)
    } else {
      MessageFormat.format(messageTemplate, targetFile, status)
    }
  }
}
