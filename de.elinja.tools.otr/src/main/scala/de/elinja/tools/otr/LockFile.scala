package de.elinja.tools.otr

import java.nio.channels.FileLock
import java.nio.channels.FileChannel
import java.io.File
import java.io.RandomAccessFile
import java.io.IOException
import java.nio.file.Files

class LockFile(val configName: String, val targetDir: String) {
  var lock: FileLock = null
  var lockFileChannel: FileChannel = null
  var lockFile: File = null

  def aquire() = {
    lockFile = new File(targetDir + "/" + configName + ".lck")
    lockFile.deleteOnExit()

    lockFileChannel = new RandomAccessFile(lockFile, "rw").getChannel();
    lock = lockFileChannel.tryLock();
    if (lock == null) {
      throw new IOException("Cannot aquire lock of file: " + lockFile.getName());
    }
  }

  def release() = {
    lock.release();
    lockFileChannel.close();
    Files.deleteIfExists(lockFile.toPath())
  }
}
