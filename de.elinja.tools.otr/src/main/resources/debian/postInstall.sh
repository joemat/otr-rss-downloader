#!/bin/sh

# Create a configuration file from the example.conf is not configuration file is there yet
if [ ! -f /etc/otr-rss-downloader/otr-rss-downloader.conf ]
then
    cp /etc/otr-rss-downloader/example.conf /etc/otr-rss-downloader/otr-rss-downloader.conf
fi
