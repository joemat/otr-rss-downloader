## A simple downloader for RSS feeds from onlinetvrecorder.com

[ ![Codeship Status for joemat/otr-rss-downloader](https://app.codeship.com/projects/4e5b6e10-4b82-0134-3be7-46e1ee2bdbc6/status?branch=master)](https://app.codeship.com/projects/169947) [![Build Status](https://semaphoreci.com/api/v1/projects/57b30526-c5a5-4b9e-a6d9-43794c8d4748/679218/badge.svg)](https://semaphoreci.com/joemat/otr-rss-downloader) [![Run Status](https://api.shippable.com/projects/588ba43e5e71000f00187b32/badge?branch=master)](https://app.shippable.com/bitbucket/joemat/otr-rss-downloader)[![Coverage Badge](https://api.shippable.com/projects/588ba43e5e71000f00187b32/coverageBadge?branch=master)](https://app.shippable.com/bitbucket/joemat/otr-rss-downloader)[ ![Download](https://api.bintray.com/packages/joemat/maven/de.elinja.tools.otr/images/download.svg) ](https://bintray.com/joemat/maven/de.elinja.tools.otr/_latestVersion)

### Download

A prebuild fat jar including all dependencies can be downloaded from bintray: [ ![Download](https://api.bintray.com/packages/joemat/maven/de.elinja.tools.otr/images/download.svg) ](https://bintray.com/joemat/maven/de.elinja.tools.otr/_latestVersion)


### Build

To build it yourself run

```
git clone https://joemat@bitbucket.org/joemat/otr-rss-downloader.git
cd otr-rss-downloader
./gradlew clean build
```

### Run

To run it call:

```
java -jar de.elinja.tools.otr/build/output/otr-rss-downloader-0.9.3/otr-rss-downloader.jar <yourconfiguration.conf>
```
*Hint:* Replace `-0.9.3` with the correct version no.

If you have downloaded/copied the jar file you probably need to adjust the path.

### Build and use a debian package

This command builds a debian package:

```
./gradlew buildDeb
```

To install it run:

```
dpkg -i de.elinja.tools.otr/build/distributions/otr-rss-downloader_0.9.3-1_all.deb
```

After installation the downloader is started with `/usr/bin/otr-rss-downloader <yourconfiguration.conf>` after installation.

### Example configuration

Get the url from [RSS-Feeds link on onlinetvrecorder.com](http://www.onlinetvrecorder.com/v2/index.php?go=rss).

Create a configuration file based on this example:

```
"url" : "paste-your-rss-feed-link-here",
"targetdir" : "the-directoy-to-save-the-movies"

"smtp" : {

        "server" : "smtp.example.com",
        "port" : 587,
        "needsAuth" : true,
        "user" : "sender@example.com",
        "password" : "secret",
        "enablestarttls" : true,

        "from" : "sender@example.com",
        "to" : "you@example.com",
}
```

This example configuration can also be downloaded, click on [example.conf](example.conf).

To disable email sending remove the smtp settings block from the configuration file.

Default values are configured in [src/main/resources/reference.conf](src/main/resources/reference.conf).


## Developing

### IDE

The [ide](ide/) subpackage contains the (goomph)[https://github.com/diffplug/goomph] gradle build file to setup a ide for development.

Run `./gradlew ide` and it will download and start an Eclipse with Scala IDE for development. The project is automatically created.


Sometimes the `ide` task fails with this error

```
:ide:ideSetupWorkspace FAILED

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':ide:ideSetupWorkspace'.
> java.lang.NoClassDefFoundError: org/scalaide/core/internal/logging/StreamRedirect$$anonfun$1$$anonfun$apply$1

* Try:
Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output.

BUILD FAILED
```

This seems to be an issue with goomph, but eclipse is nevertheless setup correctly. Simply run `ide/eclipse-ide/eclipse` and Eclipse will start with all settings applied as expected.

### Subsequent starts of eclipse

Run `ide/eclipse-ide/eclipse` to start your eclipse once it is setup.

### Unit-Tests

Run the ScalaUnit tests using

```
./gradlew test

```

To create a coverage report run

```
./gradlew testScoverage reportScoverage
```

and find the report in `de.elinja.tools.otr/build/reports/scoverage/index.html`

